import argparse
import asyncio
import websockets
import json

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRelay

pcs = set()
videoSource = ''
videosrc = None
relay = MediaRelay()

async def handler(websocket):
    while True:
        try:
            msg = await websocket.recv()
        except websockets.ConnectionClosed:
            print(f"Websocket connection terminated")
            break

        await handleSocketMessage(msg, websocket)

async def createPeerConnection(offer):
    pc = RTCPeerConnection()
    pcs.add(pc)

    global videoSource
    global videosrc

    if videosrc is None:
        videosrc = MediaPlayer(videoSource)

    video = relay.subscribe(videosrc.video)

    pc.addTrack(video)

    @pc.on("connectionstatechange")
    async def on_connectionstatechange():
        if pc.connectionState == "failed" or pc.connectionState == "closed":
            await pc.close()
            pcs.discard(pc)

    await pc.setRemoteDescription(offer)

    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    return pc.localDescription

async def handleOffer(msg, websocket):
    offer = RTCSessionDescription(sdp=msg['sdp']['sdp'], type=msg['sdp']['type'])
    answer = await createPeerConnection(offer)
    await websocket.send(json.dumps({'type': 'answer', 'sdp': {'sdp': answer.sdp, 'type': answer.type}}))

async def handleSocketMessage(msg, websocket):
    try:
        msg = json.loads(msg)
    except:
        print(f"Failed to parse json message: {msg}")
        return

    msg_handler = {
        'offer': handleOffer,
    }
    if msg['type'] in msg_handler:
        await msg_handler[msg['type']](msg, websocket)
    else:
        print("Unknown message received")

async def registerClient(websocket):
    await websocket.send(json.dumps({'type': 'camera-registration'}))

async def startClient(host, port):
    socketURL = f'ws://{host}:{port}/ws'
    async with websockets.connect(socketURL) as websocket:
        await registerClient(websocket)
        await handler(websocket)

def main(args):
    global videoSource
    videoSource = args.source
    asyncio.get_event_loop().run_until_complete(startClient(args.host, args.port))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Camera WebRTC client"
    )
    parser.add_argument(
        "--source", type=str, default='/dev/video0', help="Video source"
    )
    parser.add_argument(
        "--host", type=str, default='127.0.0.1', help="Signaling server hostname"
    )
    parser.add_argument(
        "--port", type=int, default=8080, help="Signaling server port"
    )
    args = parser.parse_args()
    main(args)
