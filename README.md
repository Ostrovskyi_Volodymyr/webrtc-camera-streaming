## WebRTC Camera client

Based on [aiortc server example](https://github.com/aiortc/aiortc/tree/main/examples/server). Custom signaling server based on web sockets is also used.
___
## Environment preparation
```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ python3 -m pip install -r requirements.txt
```
___
## Launch
First (server) console:
```
$ python3 server/server.py
```
Second (client) console:
```
$ python3 client/camera_client.py
```
