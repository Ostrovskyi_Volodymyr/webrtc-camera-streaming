import logging
from client_service import *
from aiohttp import web

logging.basicConfig(
        format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S')

logging.Formatter(
    fmt='%(asctime)s.%(msecs)03d',
    datefmt='%Y-%m-%d,%H:%M:%S'
)

async def handleOffer(msg: dict, ws: web.WebSocketResponse):
    logging.info(f"Receive browser offer")
    logging.info(f"Add new client by offer (browser client)")
    addBrowserClient(ws, msg['sdp'])

    if isCameraReady():
        await sendBrowserOffer()

async def handleCameraRegistration(msg: dict, ws: web.WebSocketResponse):
    logging.info(f"Add new client by registration (camera client)")
    addCameraClient(ws)

    if isBrowserReady():
        await sendBrowserOffer()

async def handleAnswer(msg: dict, ws: web.WebSocketResponse):
    logging.info(f"Send camera answer")

    if isBrowserReady():
        await sendCameraAnswer(msg['sdp'])

async def handleSocketMessage(msg: dict, ws: web.WebSocketResponse):
    msg_handler = {
        'offer': handleOffer,
        'camera-registration': handleCameraRegistration,
        'answer': handleAnswer
    }
    if msg['type'] in msg_handler:
        await msg_handler[msg['type']](msg, ws)
    else:
        logging.error("Unknown message received")

async def wsHandler(ws: web.WebSocketResponse):
    while True:
        try:
            msg = await ws.receive_json()
        except:
            break
        await handleSocketMessage(msg, ws)
