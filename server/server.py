import argparse
import asyncio
import json
import logging
import os
from aiohttp import web
from websocket import wsHandler

ROOT = os.path.dirname(__file__)

async def index(request):
    content = open(os.path.join(ROOT, "../web/index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)

async def javascript(request):
    content = open(os.path.join(ROOT, "../web/client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)

async def websocket_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    await wsHandler(ws)
    logging.info('websocket connection closed')
    return ws

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="WebRTC audio / video / data-channels demo"
    )
    parser.add_argument(
        "--host", default="0.0.0.0", help="Host for HTTP server (default: 0.0.0.0)"
    )
    parser.add_argument(
        "--port", type=int, default=8080, help="Port for HTTP server (default: 8080)"
    )
    parser.add_argument("--verbose", "-v", action="count")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    app = web.Application()
    app.router.add_get("/", index)
    app.router.add_get("/client.js", javascript)
    app.router.add_get("/ws", websocket_handler)
    web.run_app(
        app, access_log=None, host=args.host, port=args.port
    )
