import websockets
import json
from aiohttp.web import WebSocketResponse

clients = {}
BROWSER_CLIENT='browser'
CAMERA_CLIENT='camera'

class Client:
    def __init__(self, ws: WebSocketResponse):
        self.ws = ws
        self.sdp = None

def addBrowserClient(ws: WebSocketResponse, sdp: dict):
    client = Client(ws)
    client.sdp = sdp
    clients[BROWSER_CLIENT] = client

def addCameraClient(ws: WebSocketResponse):
    client = Client(ws)
    clients[CAMERA_CLIENT] = client

def isBrowserReady():
    return BROWSER_CLIENT in clients

def isCameraReady():
    return CAMERA_CLIENT in clients

async def _clientSend(client: str, msg: dict):
    await clients[client].ws.send_json(msg)

async def sendBrowserOffer():
    await _clientSend(
        CAMERA_CLIENT,
        {
            'type': 'offer',
            'sdp': clients[BROWSER_CLIENT].sdp
        }
    )

async def sendCameraAnswer(answer):
    await _clientSend(
        BROWSER_CLIENT,
        {
            'type': 'answer',
            'sdp': answer
        }
    )
