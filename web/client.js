const socketURL = `ws://${window.location.host}/ws`;
var signal_socket = new WebSocket(socketURL);
var peerConnection = null;

var dataChannelLog = document.getElementById('data-channel'),
    iceConnectionLog = document.getElementById('ice-connection-state'),
    iceGatheringLog = document.getElementById('ice-gathering-state'),
    signalingLog = document.getElementById('signaling-state');

async function handleVideoAnswerMsg(msg) {
    document.getElementById('answer-sdp').textContent = msg.sdp.sdp;
    var desc = new RTCSessionDescription(msg.sdp);
    await peerConnection.setRemoteDescription(desc).catch(reportError);
}

function processSignalMessage(evt) {
    var msg = JSON.parse(evt.data);

    switch(msg.type) {
      case "answer":
        handleVideoAnswerMsg(msg);
            break;
        break;
        default:
            console.error("Unknown message received!");
    }
}
signal_socket.onmessage = processSignalMessage;

function sendToServer(message) {
    signal_socket.send(JSON.stringify(message));
}

function signalServerSendOffer(sdp) {
    sendToServer({type: 'offer', sdp: sdp});
}

function reportError(errMessage) {
    console.error(`Error ${errMessage.name}: ${errMessage.message}`);
}

function setPeerEvents() {
    peerConnection.onnegotiationneeded = () => {
        console.log('negotiate');
        peerConnection
        .createOffer()
        .then((offer) => peerConnection.setLocalDescription(offer))
        .then(function () {
            // wait for ICE gathering to complete
            return new Promise(function(resolve) {
                if (peerConnection.iceGatheringState === 'complete') {
                    resolve();
                } else {
                    function checkState() {
                        if (peerConnection.iceGatheringState === 'complete') {
                            peerConnection.removeEventListener('icegatheringstatechange', checkState);
                            resolve();
                        }
                    }
                    peerConnection.addEventListener('icegatheringstatechange', checkState);
                }
            });
        }).then(() => {
            document.getElementById('offer-sdp').textContent = peerConnection.localDescription.sdp;
            signalServerSendOffer(peerConnection.localDescription);
        })
        .catch(reportError);
    };
    peerConnection.ontrack = (event) => {
        if (event.track.kind == 'video') {
            document.getElementById('video').srcObject = event.streams[0];
        }
    };
    // register some listeners to help debugging
    peerConnection.onicegatheringstatechange = () => {
        iceGatheringLog.textContent += ' -> ' + peerConnection.iceGatheringState;
    }
    iceGatheringLog.textContent = peerConnection.iceGatheringState;

    peerConnection.oniceconnectionstatechange = () => {
        iceConnectionLog.textContent += ' -> ' + peerConnection.iceConnectionState;
    }
    iceConnectionLog.textContent = peerConnection.iceConnectionState;

    peerConnection.onsignalingstatechange = () => {
        signalingLog.textContent += ' -> ' + peerConnection.signalingState;
    }
    signalingLog.textContent = peerConnection.signalingState;
}

function createPeerConnection() {
    peerConnection = new RTCPeerConnection({
    sdpSemantics: 'unified-plan',
      iceServers: [
        // Information about ICE servers - Use your own!
        {
          urls: ['stun:stun.l.google.com:19302'],
        },
      ],
    });

    setPeerEvents();
    peerConnection.addTransceiver('video');
}

function start() {
    document.getElementById('start').style.display = 'none';

    createPeerConnection();

    document.getElementById('media').style.display = 'block';

    document.getElementById('stop').style.display = 'inline-block';
}

function stop() {
    document.getElementById('start').style.display = 'inline-block';
    closeVideoCall();
    document.getElementById('stop').style.display = 'none';
}

function closeVideoCall() {
    if (peerConnection) {
        peerConnection.onnegotiationneeded = null;
        peerConnection.ontrack = null;
        peerConnection.oniceconnectionstatechange = null;
        peerConnection.onsignalingstatechange = null;
        peerConnection.onicegatheringstatechange = null;
        peerConnection.close();
        peerConnection = null;
    }
    document.getElementById('media').style.display = 'none';
}
